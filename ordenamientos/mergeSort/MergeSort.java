import java.util.Arrays;

public class MergeSort {

    public static void main(String[] args) {
        System.out.println("\tMergeSort");
        int[] A = {12, 9, 3, 7, 14, 11, 6, 2, 10, 5};
        System.out.println("Before:");
        System.out.println(Arrays.toString(A));        

        mergeSort(A,0,A.length -1);
        
        System.out.println("After: ");
        System.out.println(Arrays.toString(A));
        
        // int[] A = {32,45,50, 7, 40};
        // merge(A,0,2,4);
        // System.out.println("Fuera de la funcion:");
        // System.out.println(Arrays.toString(A));
    }

    public static void mergeSort(int[] A, int p, int q) {
        if (p < q) {
            int m = (p + q) / 2;
            mergeSort(A, p, m);
            mergeSort(A, m+1, q);
            merge(A, p, m, q);
        }
    }

    public static void merge(int[] A, int p, int m, int q) {
        int i,j,w;
         // Tratando de calcular el tamanio exacto, funciona simpre arreglo derecho
        // ,en lado izquierdo faltan poco detalles, pero si funciona
        // int[] aux = new int[(m+1) + ((q+1)-(m+1))];
        // Tamanio generico del tamanio original, desvantaja es que aveces no se ocupara todo
        // int[] aux = new int[A.length];
        // Calculando el tamanio del arreglo derecho y izquierdo para obtener el tamanio total
        int n1 = m - p + 1;
        int n2 = q - m;
        int[] aux = new int[n1+n2];
        System.out.println("len aux:" + aux.length);
        i = p;
        j = m+1;
        w = 0;

        // Comparamos elemento a elemento de los arreglos generados para
        // decidir quien ingresara primero al arreglo aux
        while (j <= q && i <= m) {
            if (A[i] > A[j]) {
                aux[w] = A[j];
                j++;
                
            } else {
                aux[w] = A[i];
                i++;                
            }
            w++;
        }

        // Vaciamos los elementos que faltan (que por cierto ya estan ordenados entre ellos)
        while( i <= m && j > q) {
            aux[w] = A[i];
            w++;
            i++;
        }

        while( j <= q && i > m) {
            aux[w] = A[j];
            w++;
            j++;
        }

        // Copiamos los elemntos ya ordenados de aux al
        // arreglo original para actualizarlos, ya que A apunta por
        // referencia al arreglo original
        for (int u = p, r=0; u <= q; u++,r++) {
            A[u] = aux[r];
        }
        System.out.println(Arrays.toString(aux));
    }
}
