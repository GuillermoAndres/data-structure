import java.util.Arrays;

public class QuickSort2 {

    public static void main(String[] args) {
        // int[] A = {35,33,42,10,14,19,27,44,26,31};
        int[] A = {50, 23, 9, 18, 61, 32};
        System.out.println("Before: ");
        System.out.println(Arrays.toString(A));
        //quickSort(A, 0, A.length - 1);
        particion2(A,0, A.length - 1);
        System.out.println("After: ");
        System.out.println(Arrays.toString(A));

    }

    public static void quickSort(int[] A,int p, int r) {
        if (p < r) {
            int q = particion2(A, p, r);
        }
    }

    public static int particion2(int[] A, int p,int r) {
        int i = 0, j = r-1;

        while( i < j) {
            if (A[i] > A[r] && A[j] < A[r]) {
                swap(A,i, j);
                i++;
                
                System.out.println(Arrays.toString(A));
            } else if (A[i] > A[r] && A[j] > A[r]) {
                j--;                
            } else if (A[i] < A[r] && A[j] < A[r]) {
                i++;
            } else if (A[i] < A[r] && A[j] > A[r]) {
                i++;
                j--;
            }
        }
        swap(A, i, r);
        return i;
    }

    public static void swap(int[] A, int i, int j) {
        int aux = A[i];
        A[i] = A[j];
        A[j] = aux;
    }
}
