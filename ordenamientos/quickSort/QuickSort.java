import java.util.Arrays;

public class QuickSort {

    
    public static void main(String[] args) {
        // int[] A = {50, 23, 9, 18, 61, 32};        
        int[] A = {10, 80, 30, 90, 40, 50, 70};        
        System.out.println("Before: ");
        System.out.println(Arrays.toString(A));
        quickSort(A, 0, A.length - 1);
        System.out.println("After: ");
        System.out.println(Arrays.toString(A));
    }

    public static void quickSort(int[] A, int p, int r) {
        if (p < r) {
            int q = particion(A, p, r);
            quickSort(A, p, q-1);
            quickSort(A, q+1, r);
        }
    }

    public static int particion(int[] A, int p, int r) {
        int i=0, j=0;
        while(j < r) {
            if (A[j] < A[r]) {
                swap(A, i, j);
                i++;
            }
            j++;
        }
        swap(A, i, r);
        return i;
    }

    public static void swap(int[] A, int i, int j) {
        int aux = A[i];
        A[i] = A[j];
        A[j] = aux;
    }

    
}
