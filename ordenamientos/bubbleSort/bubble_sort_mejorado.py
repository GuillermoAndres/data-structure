

def bubble_sort(array,n):
    flag = True
    i = 0
    pasadas = 0
    while i < n-1 and flag:
        flag = False
        for j in range(n-1-pasadas):
            if(array[j] > array[j+1]):                
                tmp = array[j]
                array[j] = array[j+1]
                array[j+1] = tmp
                flag = True
        # A partir de esta linea ya ordeno un numero.
        pasadas += 1
        # print(f'Pasada {pasadas}: {array}')
        i += 1
            

def main():
    print("\t Bubble sort mejorado in Python")
    # array = [9,21,4,40,10,35]
    array = [9,21,4,40,10,35,23,12,231,11,23,1334,63,2,8] # Peor casos
    # array = [1,2,3,4,5,6,7,8,9,10,11,12]  # Mejor casos    
    n = len(array)
    print("Before:")
    print(array)
    bubble_sort(array, n)
    print("Later:")
    print(array)


    
if __name__ == "__main__":
    main()
        
    
