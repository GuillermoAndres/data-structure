/**
   Complejidad: (n-1)(n-1) = n^2 -2n +1
*/
import java.util.Arrays;

public class BubbleSort {

    public static void main(String[] args) {
        int[] array = {9,21,4,40,10,35};
        System.out.println("\t Bubble sort");
        System.out.println("Before: ");
        System.out.println(Arrays.toString(array));
        for (int i = 0; i < array.length-1; i++) 
            for (int j = 0; j < array.length-1; j++) {
                if (array[j] > array[j+1]) {
                    // Intercambio
                    int tmp = array[j];
                    array[j] = array[j+1];
                    array[j+1] = tmp;
                }
            }
        System.out.println("Later: ");
        System.out.println(Arrays.toString(array));
    }
}
