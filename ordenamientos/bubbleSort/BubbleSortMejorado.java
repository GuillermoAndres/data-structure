/**
   Complejidad
   Mejor de los casos: O(n)
   Peor de los casos: O(n^2)
*/
import java.util.Arrays;
    
public class BubbleSortMejorado {

    public static void main(String[] args) {
        //int[] array = {9,21,4,40,10,35};
        int[] array = {9,21,4,40,10,35,23,12,231,11,23,1334,63,2,8}; // Peor casos
        //int[] array = {1,2,3,4,5,6,7,8,9,10,11,12};  // Mejor casos
        int n = array.length;
        boolean flag = true;
        int pasadas = 0;
        System.out.println("\t BubbleSort");
        System.out.println("Before:");
        System.out.println(Arrays.toString(array));
        System.out.println("Tamaño: " + array.length);
        for (int i = 0; i < n-1 && flag; i++){
            flag = false;
            for (int j = 0; j < (n-1)-pasadas; j++) {
                if (array[j] > array[j+1]) {
                    flag = true;
                    int tmp = array[j];
                    array[j] = array[j+1];
                    array[j+1] = tmp;
                }
            }            
            System.out.printf("Pasada %d: ",pasadas+1);
            System.out.println(Arrays.toString(array));
            // Una vez terminado este ciclo ya se ordeno un numero.
            pasadas += 1;
        }
        System.out.println("Later:");
        System.out.println(Arrays.toString(array));
    }
}
