
# Algortimo Bubble sort 

#La técnica utilizada se denomina ordenación por burbuja u ordenación por hundimiento y es tan fácil como
#comparar todos los elementos de una lista contra todos, si se cumple que uno es mayor a otro entonces se
#intercambia de posición. Los valores más pequeños suben o burbujean hacia la cima o parte superior de la lista,
#mientras que los valores mayores se hunden en la parte inferior.

# input: 23,12,44,12,5,2,435,12,83
# output: 

print("\t Bubble sort")
array = [9,21,4,40,10,35]
n = len(array) # Obtenemo el tamaño de nuestro arreglo
print("Before: ")
print(array)
# Algorithm 
for i in range(n-1):
    for j in range(n-1):
        if array[j] > array[j+1]:
            # intercambio
            tmp = array[j]
            array[j] = array[j+1]
            array[j+1] = tmp

print("Later: ")
print(array)
    
    


    

